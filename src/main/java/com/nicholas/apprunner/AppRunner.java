package com.nicholas.apprunner;

import com.nicholas.custom_exception.ChooseNumberException;
import com.nicholas.logics.ChooseNumber;
import com.nicholas.logics.TriangleCreator;
import java.util.Scanner;

public class AppRunner {
    public static void main(String[] args) throws ChooseNumberException {

        System.out.println("Introduceti numarul de randuri pentru tringhiul ce urmeaza a fi format:");
        Scanner nr = new Scanner(System.in);
        int z = nr.nextInt();
        int[][] pascal = new int[z][z];


        TriangleCreator obj = new TriangleCreator();
        pascal = obj.createTriangle(z);

        System.out.println("Triunghiul dumneavoastra arata asa: ");
        obj.displayTriangle(pascal);

        ChooseNumber obj2 = new ChooseNumber();
        System.out.println("Introduceti numarul randului: ");
        int x = nr.nextInt();
        System.out.println("Introduceti numarul coloanei: ");
        int y = nr.nextInt();

        obj2.showNumber(pascal, x, y);


    }
}

