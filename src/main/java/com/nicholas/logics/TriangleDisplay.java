package com.nicholas.logics;

public class TriangleDisplay {

    public void displayTriangle(int[][] pascal) {
        for (int x = 0; x < pascal.length; x++) {
            for (int y = 0; y < pascal[x].length; y++) {
                if (pascal[x][y] < 10) {
                    System.out.print("    |     " + pascal[x][y]);
                } else {
                    System.out.print("    |    " + pascal[x][y]);
                }
            }
            System.out.println("  ");
        }
    }

}
