package com.nicholas.logics;

import com.nicholas.custom_exception.ChooseNumberException;

public class ChooseNumber {

    public void showNumber(int[][] matrix, int x, int y) throws ChooseNumberException {

//        System.out.println("Introduceti numarul randului: ");
//        Scanner nr = new Scanner(System.in);
//        int x = nr.nextInt();
//        System.out.println("Introduceti numarul coloanei: ");
//        int y = nr.nextInt();
        try {
//            if (matrix[x - 1][y - 1] != 0) {
                System.out.println("Pe randul " + x + " si coloana " + y + " se afla numarul " + matrix[x - 1][y - 1] + ".");
//            } else {
//                System.out.println("Pozitia aleasa este in afara tringhiului nostru.");
//            }
        } catch (Exception e) {
            if (matrix[x - 1][y - 1] == 0) {
                throw new ChooseNumberException("Parametrii incorecti");
            }
        }
    }
}


