package com.nicholas.logics;

import java.util.Scanner;

public class TriangleCreator extends TriangleDisplay {

    private int[][] pascal;

    public int [][] createTriangle(int z) {

//        System.out.println("Introduceti numarul de randuri pentru tringhiul ce urmeaza a fi format:");
//        Scanner nr = new Scanner(System.in);
//        int z = nr.nextInt();

        pascal = new int[z][z];

        for (int x = 0; x < z; x++) {
            for (int y = 0; y <= x; y++) {
                if ((y == 0) || (y == z - 1)) {
                    pascal[x][y] = 1;
                } else {
                    pascal[x][y] = pascal[x - 1][y - 1] + pascal[x - 1][y];
                }
            }
        }

        for (int x = 0; x < z; x++) {
            for (int y = x + 1; y < z; y++) {
                pascal[x][y] = -0;
            }
        }
        return pascal;
    }

}

