package com.nicholas.custom_exception;

public class ChooseNumberException extends Exception {

    public ChooseNumberException(String e) {
        super(e);
    }

}
