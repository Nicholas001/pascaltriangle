package com.nicholas.logics;

import org.junit.*;

public class TriangleDisplayTest {

    @BeforeClass
    public static void beforeClass() {
        System.out.println("BeforeClass was called!");
    }

    @Before
    public void setUp() {
        System.out.println("SetUp was called!");
    }

    @Test
    public void displayTriangleTest() {
        TriangleCreator obj = new TriangleCreator();
        obj.displayTriangle(obj.createTriangle(5));

    }

    @After
    public void tearDown() {
        System.out.println("TearDown was called!");
    }
}
