package com.nicholas.logics;

import com.nicholas.custom_exception.ChooseNumberException;
import org.junit.*;

public class ChooseNumberTest {

    @BeforeClass
    public static void beforeClass() {
        System.out.println("BeforeClass was called!");
    }

    @Before
    public void setUp() {
        System.out.println("SetUp was called!");
    }

    @Test
    public void showNumberTest() throws ChooseNumberException {
        TriangleCreator obj = new TriangleCreator();
        ChooseNumber obj2 = new ChooseNumber();
        obj2.showNumber(obj.createTriangle(5), 3, 2);
    }

    @After
    public void tearDown() {
        System.out.println("TearDown was called!");
    }
}
